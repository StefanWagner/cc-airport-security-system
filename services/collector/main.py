from flask import Flask, jsonify, request, abort, Response
import requests

app = Flask(__name__)

bind_to = {'hostname': "0.0.0.0", 'port': 2700}
image_analysis_destination = "http://image-analysis-service:2200/frame"
section_destination = "http://section-service:2400/persons"
face_recognition_destination = "http://face-recognition-service:2300/frame"
alert_destination = "http://alert-service:2500/alerts"


@app.route('/frame', methods=['POST'])
def frame():
  if request.method == 'POST':
    if (request.is_json == True):
      forward_to_image_analysis(request.json)
      forward_to_face_recognition(request.json)
      return Response("", status=200)
  return abort(400)


def forward_to_image_analysis(request_json):
  request_json['destination'] = section_destination
  try:
    res = requests.post(image_analysis_destination, json=request_json)
    return res.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


def forward_to_face_recognition(request_json):
  request_json['destination'] = alert_destination
  try:
    res = requests.post(face_recognition_destination, json=request_json)
    return res.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


if __name__ == "__main__":
  app.run(host=bind_to['hostname'], port=int(bind_to['port']), debug=True)
