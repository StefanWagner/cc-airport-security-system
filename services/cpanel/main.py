from flask import Flask, jsonify, request, abort, Response
import requests

app = Flask(__name__)

bind_to = {'hostname': "0.0.0.0", 'port': 2800}


@app.route('/cameras', methods=['GET'])
def cameras():
  if request.method == 'GET':
    return create_list_of_cameras()
  return abort(400)


def create_list_of_cameras():
  response = {
    "cameras": [
      {
        "id": 1,
        "event": "entry",
        "section": 1,
        "url": "http://camera-service:2100/config" # portnumber
      },
      {
        "id": 2,
        "event": "exit",
        "section": 1,
        "url": "http://camera-service:2100/config" # portnumber
      }
    ]
  }
  return response


@app.route('/cameras/<id>/<value>', methods=['GET', 'PUT', 'POST'])
def camera(id, value):
  if request.method == 'GET':
    return forward_cameras_get(id, value)
  if request.method == 'PUT':
    return forward_cameras_put(id, value, request.json)
  if request.method == 'POST':
    return forward_cameras_post(id, value, request.json)
  return abort(400)


def forward_cameras_get(id, value):
  try:
    response = requests.get("http://camera-service:2100/" + value)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


def forward_cameras_put(id, value, body):
  try:
    response = requests.put("http://camera-service:2100/" + value, json=body)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


def forward_cameras_post(id, value, body):
  try:
    response = requests.post("http://camera-service:2100/" + value, json=body)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


@app.route('/section/<id>/persons', methods=['GET', 'POST'])
def section(id):
  if request.method == 'GET':
    value = "/persons?from=" + request.args.get('from') + "&to=" + request.args.get('to')
    if "aggregate" in request.args:
      value += "&aggregate=" + request.args.get("aggregate")
    if "event" in request.args:
      value += "&event=" + request.args.get("event")
    return forward_section_get(id, value)
  if request.method == 'POST':
    value = "/persons"
    return forward_section_post(id, value, request.json)
  return abort(400)


def forward_section_get(id, value):
  try:
    response = requests.get("http://section-service:2400/" + value)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


def forward_section_post(id, value, body):
  try:
    response = requests.post("http://section-service:2400/" + value, json=body)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


@app.route('/alerts/<id>/alerts', methods=['GET', 'POST'])
def alert(id):
  if request.method == 'GET':
    value = "/alerts?from=" + request.args.get('from') + "&to=" + request.args.get('to')
    if "aggregate" in request.args:
      value += "&aggregate=" + request.args.get("aggregate")
    if "event" in request.args:
      value += "&event=" + request.args.get("event")
    return forward_alerts_get(id, value)
  if request.method == 'POST':
    value = "/alerts"
    return forward_alerts_post(id, value, request.json)
  return abort(400)


def forward_alerts_get(id, value):
  try:
    response = requests.get("http://alert-service:2500/" + value)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


def forward_alerts_post(id, value, body):
  try:
    print("hier")
    response = requests.post("http://alert-service:2500/" + value, json=body)
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


@app.route('/alerts/<id>/alerts/<uuid>', methods=['GET'])
def forward_alerts_get_uuid(id, uuid):
  try:
    response = requests.get("http://alert-service:2500/" + "alerts/" + str(uuid))
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


@app.route('/alerts/<id>/alerts/<uuid>', methods=['DELETE'])
def forward_alerts_delete_uuid(id, uuid):
  try:
    response = requests.delete("http://alert-service:2500/" + "alerts/" + str(uuid))
    return response.text
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


if __name__ == "__main__":
  app.run(host=bind_to['hostname'], port=int(bind_to['port']), debug=True)
