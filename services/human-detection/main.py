from flask import Flask, jsonify, request, abort, Response
import requests

app = Flask(__name__)

bind_to = {'hostname': "0.0.0.0", 'port': 2600}
gcp_vision_api = "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyAAaPBz20vJAXQRXhI3y4YFx9FSS-HhcQw"

@app.route('/frame', methods=['POST'])
def frame():
  if request.method == 'POST':
    if (request.is_json == True):
      return forward(request.json)
  return abort(400)


def create_gcp_api_request_json(request_json):
  gcp_api_request_json = {
    "requests": [
      {
        "image": {
          "content": request_json['image']
        },
        "features": [
          {
            "maxResults": 15,
            "type": "OBJECT_LOCALIZATION"
          }
        ]
      }
    ]
  }
  return gcp_api_request_json


def create_response_json(request_json, gcp_api_response):
  counter = 0
  for localizedObjectAnnotation in gcp_api_response['responses'][0]['localizedObjectAnnotations']:
    if (localizedObjectAnnotation['name'] == "Person"):
      counter = counter + 1
  request_json['person-count'] = counter
  return request_json


def forward(request_json):
  gcp_api_request_json = create_gcp_api_request_json(request_json)
  try:
    gcp_api_response = requests.post(gcp_vision_api, json=gcp_api_request_json)
    return create_response_json(request_json, gcp_api_response.json())
  except Exception as e:
    response_json = {"error": {"code": 400, "message": str(e), "status": "DENIED"}}
    return jsonify(response_json)
  return Response("", status=200)


if __name__ == "__main__":
  app.run(host=bind_to['hostname'], port=int(bind_to['port']), debug=True)
