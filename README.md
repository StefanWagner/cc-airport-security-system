# CC Airport Security System

## Description

The task was to design and implement a airport security system which consists of 8 types of microservices:

1. Camera: represents a surveillance camera, capturing images and sending them to the system for analysis

2. ImageAnalysis: takes an image, attempts to identify human faces, and tries to estimate their age and gender

3. FaceRecognition: compares a given image against the list of persons in a predefined database for possible matches

4. Section: manage and provide statistical data for a section

5. Alert: manages and provides information about detected persons of interest

6. HumanDetection: takes an image, attempts to presence of human beings, and forwards the request if needed

7. Collector: mainly handles communication flow, e.g.:received images from Camera services and forwards them for processing to other services

8. CPanel: RestAPI for system management

## Team

Myself

## Technology

Python, Flask, Docker, Kubernetes, Google Cloud Platform

## My Responsibility

I was responsible for the implementation of microservice 6, 7 and 8.